#! /bin/bash
ANDROID_SDK=D:/AndroidDev

CMAKE_PREFIX_PATH=D:/Qt/5.15.2/android/lib/cmake/Qt5
ANDROID_NATIVE_API_LEVEL=21
Qt5_CMAKE=D:/Qt/5.15.2/android/lib/cmake

cmake -DANDROID_SDK=$ANDROID_SDK \
-DANDROID_NDK=$ANDROID_SDK/ndk/21.3.6528147 \
-DANDROID_ABI:STRING=x86 \
-DANDROID_BUILD_ABI_arm64-v8a:BOOL=OFF \
-DANDROID_BUILD_ABI_armeabi-v7a:BOOL=OFF \
-DANDROID_BUILD_ABI_x86:BOOL=ON \
-DANDROID_BUILD_ABI_x86_64:BOOL=OFF \
-DANDROID_DEPLOY_QT:FILEPATH=D:/Qt/5.15.2/android/bin/androiddeployqt.exe \
-DANDROID_STL:STRING=c++_shared \
-DANDROID_NATIVE_API_LEVEL=21 \
-DCMAKE_TOOLCHAIN_FILE=D:/AndroidDev/ndk/21.3.6528147/build/cmake/android.toolchain.cmake \
-DCMAKE_FIND_ROOT_PATH:PATH=D:/Qt/5.15.2/android \
-DQt5_DIR=$Qt5_CMAKE/Qt5 \
-DQt5Core_DIR=$Qt5_CMAKE/Qt5Core \
-DQt5Widgets_DIR=$Qt5_CMAKE/Qt5Widgets \
-DQt5Gui_DIR=$Qt5_CMAKE/Qt5Gui \
-DQt5Charts_DIR=$Qt5_CMAKE/Qt5Charts \
-DQt5LinguistTools_DIR=$Qt5_CMAKE/Qt5LinguistTools \
-DCMAKE_BUILD_TYPE="Debug" \
-DCMAKE_PROJECT_INCLUDE_BEFORE:FILEPATH=D:/Qt/Tools/QtCreator/share/qtcreator/package-manager/auto-setup.cmake \
-B android_build \
-G Ninja \

cmake --build android_build --target all
