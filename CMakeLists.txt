cmake_minimum_required(VERSION 3.21.1)
project(signalBox VERSION 0.0.1 DESCRIPTION 信号工具箱 LANGUAGES C CXX)

find_package(Qt5 COMPONENTS Core Widgets Charts LinguistTools REQUIRED)

set(execName ${PROJECT_NAME})

# rc文件的编译器, 默认会调PATH变量里找到的那个, 那个不一定就是qt gcc自带的, 因此会不兼容
set(CMAKE_RC_COMPILER D:/Qt/Tools/mingw810_32/bin/windres.exe)

set(CMAKE_CXX_STANDARD 11)

# Release编译情况下, 添加WIN32标记以禁止控制台窗口
if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
    message("build in debug")
    add_definitions(-DDEBUG=1)
else()
    message("build in resease")
    set(special_flag WIN32)
endif()

set(flex_file
    ${PROJECT_BINARY_DIR}/scanner.cpp
)

set(bison_file
    ${PROJECT_BINARY_DIR}/grammar.tab.cpp
    ${PROJECT_BINARY_DIR}/grammar.tab.hpp
)

set(automatic_gen_src
    ${flex_file}
    ${bison_file}
)

set_source_files_properties(${automatic_gen_src} GENERATED)

add_custom_target(fc
    DEPENDS ${PROJECT_SOURCE_DIR}/compiler/grammar.ypp
    BYPRODUCTS ${flex_file}
    SOURCES ${PROJECT_SOURCE_DIR}/compiler/scanner.l
    # COMMAND flex --debug --noline -o ${PROJECT_BINARY_DIR}/scanner.cpp scanner.l
    COMMAND flex --noline -o ${PROJECT_BINARY_DIR}/scanner.cpp scanner.l
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/compiler/
    COMMENT "flex build"
)

add_custom_target(bc
    DEPENDS ${PROJECT_SOURCE_DIR}/compiler/scanner.l fc
    BYPRODUCTS ${bison_file}
    SOURCES ${PROJECT_SOURCE_DIR}/compiler/grammar.ypp
    COMMAND bison --debug --no-lines --token-table -d -Wcex grammar.ypp -o ${PROJECT_BINARY_DIR}/grammar.tab.cpp
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/compiler/
    COMMENT "bison build"
)

include_directories(
    .
    ui
    calculator
    compiler
    util
    lib
    external_libs/z-fft
    ./build/signalBox_autogen/include
)
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${Qt5Core_INCLUDE_DIRS})

set(TS_FILES ui/zh_CN.ts)
set(QRC_FILES ui/res.qrc)

set(PROJECT_SOURCES
    main.cpp
    ui/mainWindow.ui
    ui/logo.rc
    ui/mainWindow.cpp
    ui/ChartView.cpp
    ui/SignalItem.cpp
    ui/SignalListWidget.cpp
    ui/SignalEditor.cpp
    ui/SignalEditorWidget.cpp
    ui/UIConfig.cpp
    ui/CodeTip.cpp
    ui/LoggerWindow.cpp
    compiler/symTable.cpp
    compiler/compiler.cpp
    compiler/ast.cpp
    compiler/libManager.cpp
    compiler/InnerLib.cpp
    calculator/calculator.cpp
    ${automatic_gen_src}
)

# 扫描工程生成和更新ts文件
# qt5_create_translation(QM_FILES ${CMAKE_SOURCE_DIR} ${TS_FILES})
# 发布ts文件, 也就是ts编译为qm
qt5_add_translation(QM_FILES ${TS_FILES})
# 添加资源文件
qt5_add_resources(PROJECT_SOURCES ${QRC_FILES})

if(ANDROID)
    add_library(${execName} SHARED
        ${PROJECT_SOURCES}
    )

    add_definitions(-DANDROID=1)
    add_definitions(-DANDROID_ABI="${ANDROID_ABI}")

    add_custom_command(TARGET ${execName}
        POST_BUILD
        WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
        COMMAND echo Deploy project to Android
        COMMAND androiddeployqt --input android_deployment_settings.json --output android-build/
    )
    add_custom_command(TARGET ${execName}
        POST_BUILD
        WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
        COMMAND cp android-build/build/outputs/apk/debug/android-build-debug.apk ${execName}.apk
    )
else()
    add_executable(${execName} ${special_flag}
        ${PROJECT_SOURCES}
    )
endif()

set_target_properties(${execName} PROPERTIES AUTOMOC TRUE AUTOUIC TRUE AUTORCC TRUE)

target_link_directories(${execName} PUBLIC lib)

target_link_libraries(${execName} ${Qt5Widgets_LIBRARIES} ${Qt5Charts_LIBRARIES})

file(GLOB libSrcFiles lib/*.cpp)

set(LIBRARY_OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR}/lib)

# 项目自带的动态库, 即内置函数库
foreach(srcFile ${libSrcFiles})
    cmake_path(RELATIVE_PATH srcFile)
    cmake_path(GET srcFile FILENAME fn)
    string(REPLACE "." ";" fn ${fn})
    list(GET fn 0 fn)
    add_library(${fn} SHARED
        ${srcFile}
    )
    target_link_directories(${fn} PUBLIC lib)
    target_link_libraries(${fn} ${Qt5Core_LIBRARIES} fft)
endforeach()

# 拷贝预置的FIR滤波器参数到目标目录
if(NOT EXISTS coefficient_demo)
message("Copy FIR coefficient")
file(COPY coefficient_demo DESTINATION .)
endif()

if(NOT EXISTS workspace_demo)
message("Copy workspace demo")
file(COPY workspace_demo DESTINATION .)
endif()

add_subdirectory(external_libs)
